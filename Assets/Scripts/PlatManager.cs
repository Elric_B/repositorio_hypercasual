using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatManager : MonoBehaviour
{
    [SerializeField] protected GameObject Line31G;
    [SerializeField] protected GameObject Line32G;
    [SerializeField] protected GameObject Line33G;
    [SerializeField] protected GameObject Line41G;
    [SerializeField] protected GameObject Line42G;
    [SerializeField] protected GameObject Line43G;
    [SerializeField] protected GameObject Line44G;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Line-3.1G"))
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(true);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(true);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }

                
        }

        if (collision.gameObject.tag == "Line-3.2G")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(true);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(true);
                Line44G.SetActive(false);
            }
        }

        if (collision.gameObject.tag == "Line-3.3G")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(true);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(true);
            }
        }

        if (collision.gameObject.tag == "Line-4.1G")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(true);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(true);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
        }

        if (collision.gameObject.tag == "Line-4.2G")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(true);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(true);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
        }

        if (collision.gameObject.tag == "Line-4.3G")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(false);
                Line32G.SetActive(true);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(true);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
        }

        if (collision.gameObject.tag == "Line-4.4G")
        {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(true);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
        }

        if(collision.gameObject.tag == "StartingPlat")
        {
            int random = Random.Range(0, 2);

            if (random == 0)
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(true);
                Line43G.SetActive(false);
                Line44G.SetActive(false);
            }
            else
            {
                Line31G.SetActive(false);
                Line32G.SetActive(false);
                Line33G.SetActive(false);
                Line41G.SetActive(false);
                Line42G.SetActive(false);
                Line43G.SetActive(true);
                Line44G.SetActive(false);
            }
        }
    }
}
