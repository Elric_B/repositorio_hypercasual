using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider TimersSlider;
    public float TotalGameTime;
    public float GameTime;
    public static bool StartTimer;
    public static bool TimesUp;
    [SerializeField] private int valorDoRelódio;

    // Start is called before the first frame update
    void Start()
    {
        StartTimer = false;
        TimesUp = false;
        TimersSlider.value = GameTime;
        TimersSlider.maxValue = TotalGameTime;

    }

    // Update is called once per frame
    void Update()
    {

        if (GameTime >= 0 && StartTimer)
        {
            TimersSlider.value -= Time.deltaTime;
        }

        if (TimersSlider.value == TimersSlider.minValue)
        {
            TimesUp = true;
        }

        if(Hourglass.MORETIME)
        {
            TimersSlider.value += valorDoRelódio;
            Hourglass.MORETIME = false;
        }

        if (Player.Dead)
            TimersSlider.value = TimersSlider.minValue;
    }
}
