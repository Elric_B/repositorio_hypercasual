using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverSound : MonoBehaviour
{
    public AudioSource GameOverAudioSource;

    // Start is called before the first frame update
    void Start()
    {
        GameOverAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayLastSong();
    }

    public void PlayLastSong()
    {
        if(Player.lastsong)
        {
            GameOverAudioSource.enabled = true;
        }
    }
}
