using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteBackground : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] protected Transform[] backgrounds;
    [SerializeField] protected float height;
    [SerializeField] protected float up_we_go;
    private int backsLenght;
    
    void Start()
    {
        backsLenght = backgrounds.Length;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform background in backgrounds)
        {
            if(background.position.y < height * backsLenght/2 * -1)
            {
                background.Translate(new Vector2(0, backsLenght * height));
            }
        }
    }

    public void BackScrollDown()
    {
        foreach (Transform background in backgrounds)
        {
            if(Player.Jumped == false)
            background.position = new Vector2(background.position.x, background.position.y - up_we_go);
        }
    }
}
