using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] protected GameObject LeftCheck;
    [SerializeField] protected GameObject RightCheck;
    [SerializeField] public GameObject PanelL;
    [SerializeField] public GameObject PanelR;
    public int currentScore;
    [SerializeField] private Text scoreText;
    //private bool JumpNow;

    // Start is called before the first frame update
    void Start()
    {
        //currentScore = currentScore - 1;
        //JumpNow = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Big Plat")
        {
            LeftCheck.SetActive(true);
            RightCheck.SetActive(true);
            StartCoroutine(WaitForChecks());

            Player.airMovement = false;
            Player.onlyOneJump = true;

            Player.Jumped = false;
        }

        if(other.gameObject.CompareTag("S�lida"))
        {
            GainScore();
        }

        if(other.gameObject.tag == "StartingPlat")
        {
            StartCoroutine(Wait());
        }
    }

    void GainScore()
    {
        currentScore++;
        scoreText.text = "Score: " + currentScore;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Big Plat")
        {
            LeftCheck.SetActive(false);
            RightCheck.SetActive(false);
            PanelL.SetActive(false);
            PanelR.SetActive(false);
            Player.airMovement = true;
            Player.onlyOneJump = false;

        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f);
        PanelL.SetActive(true);
        PanelR.SetActive(true);
    }

    IEnumerator WaitForChecks()
    {
        yield return new WaitForSeconds(0.0001f);
        PanelL.SetActive(true);
        PanelR.SetActive(true);
    }
}
