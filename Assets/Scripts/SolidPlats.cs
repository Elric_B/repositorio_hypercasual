using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidPlats : MonoBehaviour
{
    [SerializeField] public GameObject BigPlat;
    //private Collider2D MyCollider;


    // Start is called before the first frame update
    void Start()
    {
        //MyCollider = GetComponent<Collider2D>();
        //MyCollider.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BigPlat.SetActive(true);
        }
    }
}
