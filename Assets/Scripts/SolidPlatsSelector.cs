using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidPlatsSelector : MonoBehaviour
{
    [SerializeField] private GameObject SolidHourglass;
    [SerializeField] private GameObject HourglassItem;
    [SerializeField] private GameObject RegularSolid;
    [SerializeField] protected float start;
    protected bool SelectEnabled;


    // Start is called before the first frame update
    void Start()
    {
        SelectEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (SelectEnabled)
            RandomNumber();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Respawn"))
        {
                SelectEnabled = true;
                //Debug.Log("ligou");
        }
    }

    void RandomNumber()
    {
        if (SelectEnabled)
        {
            int random = Random.Range(0, 5);
            

            if (random == 0)
            {
                this.SolidHourglass.SetActive(true);
                this.HourglassItem.SetActive(true);
                this.RegularSolid.SetActive(false);
                SelectEnabled = false;
            }
            else
            {
                this.SolidHourglass.SetActive(false);
                this.HourglassItem.SetActive(false);
                this.RegularSolid.SetActive(true);
                SelectEnabled = false;
            }
            //Debug.Log("desligou");
        }

       
    }
}
