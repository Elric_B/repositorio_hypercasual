using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    private Rigidbody2D  myRigidbody;
    [SerializeField] Vector2 forcaPuloRight;
    [SerializeField] Vector2 forcaPuloLeft;
    [SerializeField] protected GameObject LeftCheck;
    [SerializeField] protected GameObject RightCheck;
    [SerializeField] GameObject deactivateBigPlat;
    [SerializeField] GameObject GameOverScreen;
    [SerializeField] GameObject GameOverPanel;
    [SerializeField] GameObject PanelL;
    [SerializeField] GameObject PanelR;
    public static bool onlyOneJump;
    public static bool airMovement;
    public static bool Jumped;
    private SpriteRenderer mySprite;
    public static bool Dead;
    public AudioClip JumpSound;
    AudioSource audiosource;
    public static bool playTheMusic;
    public static bool stopPlaying;
    public static bool lastsong;
    //private bool changePlats;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        mySprite = GetComponent<SpriteRenderer>();
        onlyOneJump = true;
        airMovement = false;
        Jumped = false;
        Application.targetFrameRate = 60;
        Dead = false;
        GameOverPanel.SetActive(false);
        audiosource = GetComponent<AudioSource>();
        playTheMusic = false;
        stopPlaying = false;
        lastsong = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (airMovement)
        {
            //myRigidbody.velocity = Vector2.zero;
            //myRigidbody.AddForce(forcaairmov, ForceMode2D.Impulse);

            //transform.position = Vector2.MoveTowards(transform.position, new Vector2(0.5f, transform.position.y), 0.7f * Time.deltaTime);
        }

        if(Timer.TimesUp)
        {
            GameOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("GameOver"))
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        Dead = true;
        GameOverScreen.SetActive(true);
        GameOverPanel.SetActive(true);
        audiosource.Stop();
        lastsong = true;
        PanelL.SetActive(false);
        PanelR.SetActive(false);
    }

    public void JumpRight()
    {
        playTheMusic = true;

        if (playTheMusic == true && stopPlaying == false)
            PlayMusic();

        Jumped = true;
        Timer.StartTimer = true;

        PlatCheckLeft.downfallleft = false;

        if (onlyOneJump == true)
        {

            myRigidbody.velocity = Vector2.zero;

            myRigidbody.AddForce(forcaPuloRight, ForceMode2D.Impulse);

            audiosource.PlayOneShot(JumpSound, 10);

            mySprite.flipX = true;

            if (PlatCheckRight.downfallright)
                deactivateBigPlat.SetActive(false);

        }

        onlyOneJump = false;

        LeftCheck.SetActive(false);
        RightCheck.SetActive(false);


    }

    public void JumpLeft()
    {
        playTheMusic = true;

        if (playTheMusic == true && stopPlaying == false)
            PlayMusic();

        Jumped = true;
        Timer.StartTimer = true;

        PlatCheckRight.downfallright = false;

        if (onlyOneJump == true)
        {

            myRigidbody.velocity = Vector2.zero;

            myRigidbody.AddForce(forcaPuloLeft, ForceMode2D.Impulse);

            audiosource.PlayOneShot(JumpSound,10);

            mySprite.flipX = false;

            if (PlatCheckLeft.downfallleft)
                deactivateBigPlat.SetActive(false);

        }

        onlyOneJump = false;

        LeftCheck.SetActive(false);
        RightCheck.SetActive(false);
    }

    public void Reload()
    {
        if(Dead)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PlayMusic()
    {
        audiosource.Play();
        stopPlaying = true;
    }
}
