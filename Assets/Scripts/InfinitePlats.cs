using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinitePlats : MonoBehaviour
{
    //tudo com duas barras foi um teste, favor n�o mexer. Vou ver se � vi�vel ainda.

    [SerializeField] protected Transform[] plats;
    [SerializeField] protected float speed;
    //[SerializeField] protected Vector2 speed2;
    [SerializeField] protected float start;
    [SerializeField] protected float limit;
    

    //private Vector3 posinicial;
    //private Vector3 posfinal;
    //[SerializeField] protected float tempoDeLerp;
    //private float tempoDeLerpAtual;
    //protected bool Plats_Go_Down;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
       foreach (Transform plat in plats)
        {
            if(plat.position.y <= limit)
            {
                plat.position = new Vector2(plat.position.x, start);
            }
        }

        //if (Plats_Go_Down == true)
        //{
        //    foreach (Transform plat in plats)
        //    {
        //        posinicial = plat.transform.position;
        //        posfinal = plat.transform.position + Vector3.down * speed;

        //        tempoDeLerpAtual += Time.deltaTime;

        //        if (tempoDeLerpAtual >= tempoDeLerp)
        //            tempoDeLerpAtual = tempoDeLerp;

        //        float TempodeMov = tempoDeLerpAtual / tempoDeLerp;
        //        plat.transform.position = Vector3.Lerp(posinicial, posfinal, TempodeMov);
        //        Plats_Go_Down = false;
        //    }
        //}
    }

    public void ScrollDown()
    {
        foreach (Transform plat in plats)
        {

            //Plats_Go_Down = true;
            if (Player.Jumped == false)
                plat.position = new Vector2(plat.position.x, plat.position.y - speed);
        }

        
    }
}
