using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlat : MonoBehaviour
{
    private Rigidbody2D myRigidbody;
    [SerializeField] private Vector2 force;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("GroundCheck"))
        {
            myRigidbody.AddForce(force, ForceMode2D.Impulse);
        }

        if(collision.gameObject.CompareTag("GameOver"))
        {
            this.gameObject.SetActive(false);
        }
    }
}
